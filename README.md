# Visor Mapa

|    Metrics    |                                                                                     Master                                                                                     |                                                                                  Develop                                                                                 |
|:-------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| CI status     | [![pipeline status](https://gitlab.com/cabildo-tf/vente/esri/webapps/cartographic-viewer/badges/master/pipeline.svg)](https://gitlab.com/cabildo-tf/vente/esri/webapps/cartographic-viewer/-/commits/master) | [![pipeline status](https://gitlab.com/cabildo-tf/vente/esri/webapps/cartographic-viewer/badges/develop/pipeline.svg)](https://gitlab.com/cabildo-tf/vente/esri/webapps/cartographic-viewer/-/commits/develop) |

Visor cartográfico

Crear enlace simbólico en la carpeta del WAB

```
mklink /D C:\inetpub\wwwroot\WebAppBuilderForArcGIS\server\apps\2 C:\inetpub\wwwroot\WebAppBuilderForArcGIS\server\apps\cartographic-viewer\src
```