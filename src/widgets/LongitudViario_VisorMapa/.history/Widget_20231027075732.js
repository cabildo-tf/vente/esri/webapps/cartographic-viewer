define(['jimu/dijit/FeaturelayerChooserFromMap', "esri/geometry/Point", "esri/geometry/geometryEngine", "esri/SpatialReference", 'jimu/dijit/LoadingShelter', "dojo/dom-class", 'dojo/on', "esri/map",
	'jimu/dijit/LayerChooserFromMapWithDropbox', 'dojo/_base/declare', "esri/dijit/LayerList", "esri/arcgis/utils", 'jimu/BaseWidget', 'jimu/BaseWidgetSetting', 'dojo/_base/lang', 'jimu/LayerInfos/LayerInfos', 'dojo/_base/array', "dojo/dom-attr",
	"esri/toolbars/draw", 'esri/layers/GraphicsLayer', "esri/layers/FeatureLayer", "esri/tasks/QueryTask", "esri/tasks/query", "esri/graphic",
	'esri/geometry/Point', 'esri/geometry/Polyline', 'esri/symbols/SimpleLineSymbol', 'esri/symbols/SimpleFillSymbol', 'esri/symbols/SimpleMarkerSymbol', 'esri/Color', "esri/tasks/GeometryService", "esri/tasks/BufferParameters", 'esri/dijit/editing/AttachmentEditor',"dojo/parser","esri/geometry/Extent","dojo/dom-construct","dojo/domReady!"],
	function (FeaturelayerChooserFromMap, Point, geometryEngine, SpatialReference, LoadingShelter, dojoClass, on, Map,
		LayerChooserFromMapWithDropbox, declare, LayerList, arcgisUtils, BaseWidget, BaseWidgetSetting, lang, LayerInfos, array, domAttr, Draw, GraphicsLayer, FeatureLayer, QueryTask, Query, Graphic, Point, Polyline, SimpleLineSymbol, SimpleFillSymbol, SimpleMarkerSymbol, Color, GeometryService, BufferParameters, AttachmentEditor, Parser, Extent, DomConstruct) {
		return declare([BaseWidget], {
			baseClass: 'jimu-widget-customwidget',
			layerEntidades: [],
			layerRelations: [],
			layerGeometria: [],
			tablaPos_base: [],
			tablaPos_ini: [],
			tablaPos_fin: [],
			layerTablas: [],
			cont: 0,
			DomRegulacion: [],
			DomLimitacion: [],
			entidadEq: "",
			id_eq: "",	
			url_viario: "",
			url_eq: "",			
			url_relation: "",
			url_doc: "",
			url_red: "",
			url_acceso: "",
			url_incidencia: "",
			url_incidencia_geom: "",
			url_incidencia_libre: "",					
			title_doc: "Documentos",	
			resultados: [],
			elementos_eq: [],
			elementos_aux: [],		
			selected_eq: [],
			selected_eq_f: [],			
			geometry_eq: "",
			draw: null,
			doc_nombre: "",
			doc_descricion: "",
			doc_idioma: "",
			doc_uso_interno: "",
			doc_editorial: "",
			doc_visible: "",
			doc_codigo_anterior: "",
			doc_nombre_anterior: "",
			doc_objectid: "",
			Graphics_eqd: null,
			graphics_globalid_doc: [],
			marcar_todos: false,			
			onOpen: function () {

			},
			onClose: function () {
				this.draw.deactivate();
				this.Graphics_eqd.clear();

				// Restablecer el selector inicial
				var selectEntidad = document.getElementById("selec_entidad");
				selectEntidad.value = "Seleccione una entidad...";
				selectEntidad.selectedIndex = 0;

				//Eliminar la tabla de registros seleccionado
				var entidadListDiv = document.getElementById('entidad_list');
				var ulElements = entidadListDiv.getElementsByTagName('ul');
				var ulArray = Array.from(ulElements);
				ulArray.forEach(function(ulElement) {
					ulElement.parentNode.removeChild(ulElement);
				});

				// Ocultar de nuevo la capa de viario
				for (var layerId in this.map._layers) {
					// Verifica si el nombre de la capa contiene "VisorCartografico"
					if (layerId.includes("VisorCartografico")){
						for(x=0; x < this.map._layers[this.config.idViario_VisorCartografico].layerInfos.length; x++){
							if(this.map._layers[this.config.idViario_VisorCartografico].layerInfos[x].name == this.config.nombre_viario){
								var valores_CapasVisibles = []
								for (var capaId in this.map._layers[this.config.idViario_VisorCartografico].visibleLayers){
									
									valores_CapasVisibles.push(this.map._layers[this.config.idViario_VisorCartografico].visibleLayers[capaId])
								}
								if(valores_CapasVisibles.includes(this.config.idNumerico_VisorCartografico_layersid)){
									for(i=0; i < this.map._layers[this.config.idViario_VisorCartografico].visibleLayers.length;i++){
										if(this.map._layers[this.config.idViario_VisorCartografico].visibleLayers[i] == this.config.idNumerico_VisorCartografico_layersid){
											this.map._layers[this.config.idViario_VisorCartografico].visibleLayers.splice(i, 1)
										}
									}
								}
							}
						}
					}
				}


				if (document.getElementById('end').style.display == 'block') {
					document.getElementById('dialogo_exit').style.display = 'none';
					this.go_inicio();
				} else if (document.getElementById('inicio').style.display == 'block') {
					document.getElementById('dialogo_exit').style.display = 'none';
				} else {
					document.getElementById('dialogo_exit').style.display = 'block';
				}
			},

			startup: function () {
				gsvc = new GeometryService(this.config.GeometryServer);
				this.Graphics_eqd = new GraphicsLayer({"id": 'glEquipamientosDoc'});
      			this.map.addLayer(this.Graphics_eqd);
				Parser.parse();				        
				this.inherited(arguments);
				var NombreEntidades = [];
				var NombreRelations = [];
				var Idioma = [];
				var IdiomaValue = [];
				var Booleano = [];
				var BooleanoValue = [];	
				
				
				//Llamar al div del combo
				let divCombo = document.getElementById("selec_entidad");

				//Rellenar el combo con los keys
				for (i = 0; i < this.config.Capas.length; i++) {
					divCombo.innerHTML += '<option value="' + this.config.Capas[i] + '">' + this.config.Capas[i] + '</option>';
				}		

				//Boton Comenzar
				var button_fc = document.getElementById('comenzar');
				button_fc.addEventListener('click', () => {					
					this.load_page(1);
				});
				
				//Button Combo Tipo Entidad	
				var combobutton = document.getElementById("selec_entidad");
		    	combobutton.addEventListener("change", () => {
		    	this.Graphics_eqd.clear();
		    	document.getElementById('combo_one_list').style.display = 'none';
				document.getElementById('entidad_list').style.display = 'none';
				document.getElementById('geometry_oblig').style.display = 'none';
				document.getElementById('select_region').style.display = 'none';
				document.getElementById('least_one_list').style.display = 'none';
				document.getElementById('layer_novisible').style.display = 'none';
				document.getElementById('layer_nogeometria').style.display = 'none';
		      	document.getElementById('option1').checked = '';
		    	document.getElementById('option1').disabled = false;		    	
		    	var EntidadSeleccionada = document.getElementById("selec_entidad").value;
				// Poner visible la capa viario al seleccionarla en el combo
				if(EntidadSeleccionada == "Viario"){
					for (var layerId in this.map._layers) {
						// Verifica si el nombre de la capa contiene "VisorCartografico"
						if (layerId.includes("VisorCartografico")){
							for(x=0; x < this.map._layers[this.config.idViario_VisorCartografico].layerInfos.length; x++){
								if(this.map._layers[this.config.idViario_VisorCartografico].layerInfos[x].name == this.config.nombre_viario){
									if(this.map._layers[this.config.idViario_VisorCartografico].visible == false){
										this.map._layers[this.config.idViario_VisorCartografico].setVisibility(true)
									}
									var valores_CapasVisibles = []
									for (var capaId in this.map._layers[this.config.idViario_VisorCartografico].visibleLayers){
										
										valores_CapasVisibles.push(this.map._layers[this.config.idViario_VisorCartografico].visibleLayers[capaId])
									}
									if(!valores_CapasVisibles.includes(this.config.idNumerico_VisorCartografico_layersid)){
										this.map._layers[this.config.idViario_VisorCartografico].visibleLayers[this.map._layers[this.config.idViario_VisorCartografico].visibleLayers.length + 1] = this.config.idNumerico_VisorCartografico_layersid
										// hacer un zoom out para que cargue bien la capa de viario
										var currentZoom = this.map.getZoom();
										if (currentZoom > 0) {
										this.map.setZoom(currentZoom - 1);
										}
									}
								}
							}
						}
					}
				}		      
		    });
				
				//Button TipoBusqueda	
				var tipomapabutton = document.getElementById("option1");
				tipomapabutton.onclick = lang.hitch(this, this.botonDraw);
				this.draw = new Draw(this.map);
				this.draw.on('draw-complete', lang.hitch(this, this.geoEquipamiento));
		    	tipomapabutton.addEventListener("click", () => {
					document.getElementById('no_list').style.display = 'none';
					var EntidadSeleccionada = document.getElementById("selec_entidad").value;
					if (EntidadSeleccionada == "") {
						 document.getElementById('combo_one_list').style.display = 'block';
						 document.getElementById('option1').checked = '';
						 document.getElementById('geometry_oblig').style.display = 'none';
						 document.getElementById('select_region').style.display = 'none';
						 document.getElementById('least_one_list').style.display = 'none';
						 document.getElementById('layer_novisible').style.display = 'none';						 
				  	} else {
				  	 	this.Graphics_eqd.clear();
						document.getElementById('geometry_oblig').style.display = 'none';
						document.getElementById('select_region').style.display = 'none';
						document.getElementById('least_one_list').style.display = 'none';
						document.getElementById('layer_novisible').style.display = 'none';         
						document.getElementById('entidad_list').innerHTML = '';
						document.getElementById('entidad_list').style.display = 'none';		         
					}
		    	});
											
				var tipomapabutton2 = document.getElementById("option2");
				tipomapabutton2.onclick = lang.hitch(this, this.botonDraw);
				this.draw = new Draw(this.map);
				this.draw.on('draw-complete', lang.hitch(this, this.geoEquipamiento));
		    	tipomapabutton2.addEventListener("click", () => {
		    	var EntidadSeleccionada = document.getElementById("selec_entidad").value;
					if (EntidadSeleccionada == "") {
						 document.getElementById('combo_one_list').style.display = 'block';
						 document.getElementById('option2').checked = '';
						 document.getElementById('geometry_oblig').style.display = 'none';
						 document.getElementById('select_region').style.display = 'none';
						 document.getElementById('least_one_list').style.display = 'none';
						 document.getElementById('layer_novisible').style.display = 'none';
						 document.getElementById('layer_nogeometria').style.display = 'none';
				  	} else {
				  	 	document.getElementById('geometry_oblig').style.display = 'none';
						document.getElementById('select_region').style.display = 'none';
						document.getElementById('least_one_list').style.display = 'none';
						document.getElementById('layer_novisible').style.display = 'none';
						document.getElementById('layer_nogeometria').style.display = 'none';
						document.getElementById('entidad_list').innerHTML = '';	         
		      		}  
		    	});

				//Boton Siguiente Formulario_1
				var button_f1 = document.getElementById('end_form_1');
				button_f1.addEventListener('click', () => {
				  this.selected_eq = [];
				  for (var i = 0; i < this.elementos_eq.length; i++) {
					  var n = i.toString();
					  var id = 'check_btn_' + n;					  
					 	if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
								this.selected_eq.push(this.elementos_eq[i]);
					  }						
				  }				  
				  if (this.selected_eq.length == 0) {
					  document.getElementById('least_one_list').style.display = 'block';			  
				  }
				  else {				  	
					  document.getElementById('least_one_list').style.display = 'none';	
					  document.getElementById('least_one_list_select').style.display = 'none';		  
					  this.load_page(2);
					  var EntidadSeleccionada = document.getElementById("selec_entidad").value;
					  this.entidadEq = EntidadSeleccionada;
		    		if (EntidadSeleccionada == 'Viario'){
		    			this.Graphics_eqd.clear();
		    			var globalid_selected ="globalid IN (";
				      for (i = 0; i < this.selected_eq.length; i++) {        
				        globalid_selected += "'"+this.selected_eq[i].features.attributes.globalid+"',"; 
				      }
					    globalid_selected += "'')";
					    var query = new Query();							
							query.where = globalid_selected;
							query.outFields = ["*"];
							query.returnGeometry = true;


							if(!this.id_eq.includes("VisorCartografico")){
								this.map._layers[this.id_eq].selectFeatures(query,FeatureLayer.SELECTION_NEW,(result_select_graphics) => {
									this.Graphics_eqd.clear();
									for (var i = 0; i < result_select_graphics.length; i++) {
										var line = new SimpleLineSymbol();
										line.setWidth(5);
										line.setColor(new Color([0, 197, 255, 1]));
										result_select_graphics[i].symbol = line;
										this.Graphics_eqd.add(result_select_graphics[i]);
										if(i == result_select_graphics.length - 1){
											this.Graphics_eqd.clear();
											for (var i = 0; i < result_select_graphics.length; i++) {
												this.Graphics_eqd.add(result_select_graphics[i]);
											}
										}							  								  	
									}	
										
									this.Graphics_eqd.redraw();	
									document.getElementById('option1').checked = '';															    	
								}
								,(rollback_select_graphics) => {
											this.Graphics_eqd.clear();
								});	

							}else if(this.map._layers[this.id_eq].layerInfos[this.config.idNumericoViario_VisorCartografico] && this.map._layers[this.id_eq].layerInfos[this.config.idNumericoViario_VisorCartografico].name == this.entidadEq){
								// Añadir capa de viario desde un FeatureServer
								var viario_FL = new FeatureLayer(this.config.url_ViarioFL,{id: "CapaViarioTemporal", outFields: ["*"]});

									this.map.addLayer(viario_FL);

									viario_FL.selectFeatures(query,FeatureLayer.SELECTION_NEW,(result_select_graphics) => {
									this.Graphics_eqd.clear();
									for (var i = 0; i < result_select_graphics.length; i++) {
									
										var line = new SimpleLineSymbol();
										line.setWidth(5);
										line.setColor(new Color([0, 197, 255, 1]));
										result_select_graphics[i].symbol = line;
										this.Graphics_eqd.add(result_select_graphics[i]);
										if(i == result_select_graphics.length - 1){
											this.Graphics_eqd.clear();
											for (var i = 0; i < result_select_graphics.length; i++) {
												this.Graphics_eqd.add(result_select_graphics[i]);
											}
										}							  								  	
									}	

									this.Graphics_eqd.redraw();	
									document.getElementById('option1').checked = '';
									if(this.map._layers["CapaViarioTemporal"]){
										this.map.removeLayer(this.map._layers["CapaViarioTemporal"])
									}													    	
								}
								,(rollback_select_graphics) => {
											this.Graphics_eqd.clear();
								});	

							}



							
							
		        	this.load_table_selected("CodigoNombre");
		        } else if (EntidadSeleccionada == this.config.format_incidencia || EntidadSeleccionada == this.config.format_red_recreativa_sistema_senalizacion) {
		        	this.load_table_selected("CodigoNombre");
		        } else if (EntidadSeleccionada == this.config.format_red_recreativa_regulacion){
		        	this.load_table_selected(this.config.format_red_recreativa_regulacion);
		      	} else if (EntidadSeleccionada == this.config.format_grupo_instalacion_recreativa_acceso_regulacion){
		        	this.load_table_selected(this.config.format_grupo_instalacion_recreativa_acceso_regulacion);
		      	} else if (EntidadSeleccionada == this.config.format_incidencia_restriccion){
		        	this.load_table_selected(this.config.format_incidencia_restriccion);
		      	} else {
		      		this.load_table_selected("globalid");
		        }
				  }          
				});

				//Boton Atras Formulario_2
				document.getElementById('end_form_21').addEventListener('click', () => {					
					document.getElementById('least_one_list').style.display = 'none';
					this.load_page(1);
					if (document.getElementById('option2').checked == true){
						this.tablaPos_base = [];
											
					} 
					if (document.getElementById('option1').checked == true){
						this.tablaPos_base = this.tablaPos_ini;						
						var globalid_selected ="globalid IN (";
			      for (i = 0; i < this.elementos_eq.length; i++) {			      	    
			        	globalid_selected += "'"+this.elementos_eq[i].features.attributes.globalid+"',"; 			        
			      }
				    globalid_selected += "'')";
				    var query = new Query();							
						query.where = globalid_selected;
						query.outFields = ["*"];
						query.returnGeometry = true;
						this.graphics_globalid_doc = [];
						for (var l = 0; l < this.Graphics_eqd.graphics.length; l++){
							this.graphics_globalid_doc.push(this.Graphics_eqd.graphics[l].attributes.globalid);
						}
						this.map._layers[this.id_eq].selectFeatures(query,FeatureLayer.SELECTION_NEW,(result_select_graphics) => {
								for (var i = 0; i < result_select_graphics.length; i++) {
								if (this.Graphics_eqd.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline'){
							  		if (this.graphics_globalid_doc.includes(result_select_graphics[i].attributes.globalid)== false){
											result_select_graphics[i].symbol = null;
									  	this.Graphics_eqd.add(result_select_graphics[i]);
									  } else {
									  	var line = new SimpleLineSymbol();
								  		line.setWidth(5);
											line.setColor(new Color([0, 197, 255, 1]));
											result_select_graphics[i].symbol = line;
									  	this.Graphics_eqd.add(result_select_graphics[i]);
										}
							  	} else {
							  		if (this.graphics_globalid_doc.includes(result_select_graphics[i].attributes.globalid)== false){
									  	result_select_graphics[i].symbol = null;
									  	this.Graphics_eqd.add(result_select_graphics[i]);
									  } else{
									  	var line = new SimpleLineSymbol();
											line.setColor(new Color([0, 197, 255, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											marker.setColor(new Color([0, 197, 255, 0.26]));
											marker.setAngle(0);
											marker.setOutline(line);
									  	result_select_graphics[i].symbol = marker;
									  	this.Graphics_eqd.add(result_select_graphics[i]);
										}
								  }
							  }									
							  this.Graphics_eqd.redraw();		    	
						}
						, (rollback_select_graphics) => {
									this.Graphics_eqd.clear();
						});					
					}
				});
			},

			
			
			load_table_buscador: function (tipo) {
				var htmlToAdd = '';
				if (this.elementos_eq.length == 0) {
					document.getElementById('no_list').style.display = 'block';
				}
				else {			
					document.getElementById('entidad_list').style.display = 'block';		
					htmlToAdd += '<ul class="list-group">';
					for (var i = 0; i < this.elementos_eq.length; i++) {
						if (tipo == "CodigoNombre") {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.codigo + " | " + this.elementos_eq[i].features.attributes.nombre + "</span></li>";
						} else if (tipo == this.config.format_red_recreativa_regulacion) {
							if (this.elementos_aux.length == 0) {
								document.getElementById('entidad_list').style.display = 'none';
								document.getElementById('no_list').style.display = 'block';
							} else {
								for (var j = 0; j < this.elementos_aux.length; j++){
									if (this.elementos_eq[i].features.attributes.red_recreativa_id == this.elementos_aux[j].features.attributes.globalid) {					
										htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.matricula + " | " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomRegulacion[this.elementos_eq[i].features.attributes.regulacion_id] + "</span></li>";
						    	}
						    }
						  }
						}	else if (tipo == this.config.format_grupo_instalacion_recreativa_acceso_regulacion) {							
							if (this.elementos_aux.length == 0) {
								document.getElementById('entidad_list').style.display = 'none';
								document.getElementById('no_list').style.display = 'block';
							} else {
								for (var j = 0; j < this.elementos_aux.length; j++){
									if (this.elementos_eq[i].features.attributes.grupo_instala_recrea_acceso_id == this.elementos_aux[j].features.attributes.globalid) {					
										htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomRegulacion[this.elementos_eq[i].features.attributes.regulacion_id] + "</span></li>";
						    	}
						    }
						  }
						} else if (tipo == this.config.format_incidencia_restriccion) {							
							if (this.elementos_aux.length == 0) {
								document.getElementById('entidad_list').style.display = 'none';
								document.getElementById('no_list').style.display = 'block';
							} else {
								for (var j = 0; j < this.elementos_aux.length; j++){
									if (this.elementos_eq[i].features.attributes.incidencia_id == this.elementos_aux[j].features.attributes.globalid) {					
										htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.codigo + " | " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomLimitacion[this.elementos_eq[i].features.attributes.limitacion] + "</span></li>";
						    	}
						    	if (this.elementos_eq[i].features.attributes.incidencia_geom_id == this.elementos_aux[j].features.attributes.globalid) {					
										htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.codigo + " | " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomLimitacion[this.elementos_eq[i].features.attributes.limitacion] + "</span></li>";
						    	}
						    	if (this.elementos_eq[i].features.attributes.incidencia_libre_id == this.elementos_aux[j].features.attributes.globalid) {					
										htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_aux[j].features.attributes.codigo + " | " + this.elementos_aux[j].features.attributes.nombre + " | " + this.DomLimitacion[this.elementos_eq[i].features.attributes.limitacion] + "</span></li>";
						    	}
						    }
						  }
						} else {
							htmlToAdd += "<li class='list-group-item'><input type='checkbox' id='check_btn_" + i + "' onclick=''>" + "<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.globalid + "</span></li>";
						}
					}
					htmlToAdd += '</ul>';
				}
				document.getElementById('entidad_list').innerHTML += htmlToAdd;
			},

			load_table_selected: function (tipo) {
				this.tablaPos_fin = [];
				document.getElementById('entidad_list_select').innerHTML = '';				
				if (this.selected_eq.length == 0) {
					document.getElementById('no_list').style.display = 'block';
				}
				else {
					var longitudViarioSelect = 0
					for(var x=0; x < this.selected_eq.length; x++){
						longitudViarioSelect += this.selected_eq[x].features.attributes.Shape__Length
					}
					var longitudViario = Number(longitudViarioSelect.toFixed(2))
					var parrafoResumen=DomConstruct.toDom("<p><strong>Longitud del viario seleccionado: </strong>" + longitudViario + " (m)");
				  	DomConstruct.place(parrafoResumen,'entidad_list_select','last');
					this.tablaPos_fin[i] = new this.Tabla_localizador(i,this.selected_eq[i].features.attributes.globalid);
					this.tablaPos_base = this.tablaPos_fin;				
				}				
			},
			
			resaltar_graphics: function (globalid,id_button,tipo) {				
					return function () {
						var id = tipo + id_button;
						if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
							for (var i = 0; i < this.Graphics_eqd.graphics.length; i++) {
						  	if (globalid == this.Graphics_eqd.graphics[i].attributes.globalid) {
									if (this.Graphics_eqd.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline'){
								  		var line = new SimpleLineSymbol();
								  		line.setWidth(5);
											line.setColor(new Color([230, 0, 0, 1]));											
									  	this.Graphics_eqd.graphics[i].setSymbol(line);										
								  	} else {
									  	var line = new SimpleLineSymbol();
											line.setColor(new Color([230, 0, 0, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setOutline(line);
											marker.setColor(new Color([230, 0, 0, 0.25]));
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											this.Graphics_eqd.graphics[i].setSymbol(marker);
									  }
						  	} else {
						  		for(var j = 0; j < this.tablaPos_base.length; j++){
						  			 var id = tipo + this.tablaPos_base[j].posicion;
						  		   if (document.getElementById(id).checked == true && this.tablaPos_base[j].globalid == this.Graphics_eqd.graphics[i].attributes.globalid)
											if (this.Graphics_eqd.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline'){
										  		var line = new SimpleLineSymbol();
										  		line.setWidth(5);
													line.setColor(new Color([0, 197, 255, 1]));								
											  	this.Graphics_eqd.graphics[i].setSymbol(line);
										  	} else {
											  	var line = new SimpleLineSymbol();
													line.setColor(new Color([0, 197, 255, 1]));
													var marker = new SimpleMarkerSymbol();
													marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
													marker.setColor(new Color([0, 197, 255, 0.26]));								
													marker.setOutline(line);						  	
											  	this.Graphics_eqd.graphics[i].setSymbol(marker);
											  }
										 }
						  		}
						  	}						  		
						 }
					}				
			},
			
			comprobar_graphics: function (globalid,id_button,tipo) {
				
					return function () {
						var id = tipo + id_button;
						if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
								for (var i = 0; i < this.Graphics_eqd.graphics.length; i++) {
									if (globalid == this.Graphics_eqd.graphics[i].attributes.globalid){
										if (this.Graphics_eqd.graphics[i].geometry.__proto__.declaredClass == 'esri.geometry.Polyline'){
											var line = new SimpleLineSymbol();
											line.setWidth(5);
											line.setColor(new Color([0, 197, 255, 1]));								
											this.Graphics_eqd.graphics[i].setSymbol(line);
								  	} else {
									  	var line = new SimpleLineSymbol();
											line.setColor(new Color([0, 197, 255, 1]));
											var marker = new SimpleMarkerSymbol();
											marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
											marker.setColor(new Color([0, 197, 255, 0.26]));								
											marker.setOutline(line);						  	
									  	this.Graphics_eqd.graphics[i].setSymbol(marker);
									  }		
									} 
								}
					  } else {
					  		for (var i = 0; i < this.Graphics_eqd.graphics.length; i++) {
									if (globalid == this.Graphics_eqd.graphics[i].attributes.globalid){										
										this.Graphics_eqd.graphics[i].setSymbol(null);
									}
								}					  	
					  }							
					}				
			},

			botonDraw: function () {
				document.getElementById('geometry_oblig').style.display = 'none';
				if(document.getElementById('option1').checked == true){
					this.Graphics_eqd.clear();
				}
				this.draw.activate('freehandpolygon');
			},
			
			geoEquipamiento: function (event) {
				if(document.getElementById('option1').checked == true){
					this.draw.deactivate();
					this.geometry_eq = event.geometry;
					document.getElementById('geometry_oblig').style.display = 'none';
					document.getElementById('select_region').style.display = 'none';
					document.getElementById('least_one_list').style.display = 'none';	
					document.getElementById('layer_novisible').style.display = 'none';	
					document.getElementById('entidad_list').innerHTML = '';
					var EntidadSeleccionada = document.getElementById("selec_entidad").value;
					this.entidadEq = EntidadSeleccionada;
					var consultas = [];				
					this.elementos_eq = [];
					var url_capa = "";
					var id_capa = "";
					var pos_entidad = 0;
					
					for(var x=0; x < this.map.itemInfo.itemData.operationalLayers.length; x++){
						for(var y=0; y < this.map.itemInfo.itemData.operationalLayers[5].layers.length; y++){
							if(this.map.itemInfo.itemData.operationalLayers[x].title == this.entidadEq){
								url_capa = this.map.itemInfo.itemData.operationalLayers[x].url
								id_capa = this.map.itemInfo.itemData.operationalLayers[x].id
								break
							}
							if(this.map.itemInfo.itemData.operationalLayers[5].layers[y].name == this.entidadEq){
								url_capa = this.config.urlViario_VisorCartografico
								id_capa = this.config.idViario_VisorCartografico
								break
							}
						}	
					}
					this.url_eq = url_capa;
					this.id_eq = id_capa;

				
					if (this.map._layers[this.id_eq] && this.map._layers[this.id_eq].visible == true || this.map._layers[this.id_eq].layerInfos[this.config.idNumericoViario_VisorCartografico] && this.map._layers[this.id_eq].layerInfos[this.config.idNumericoViario_VisorCartografico].name == this.entidadEq) {
								var query = new Query();
								query.geometry = this.geometry_eq;
								query.where = "1=1";
								query.outFields = ["*"];
								query.returnGeometry = true;																	
								var query_elements = new QueryTask(url_capa);
								consultas.push(query_elements.execute(query));
								
								Promise.all(consultas).then(results => {
									const resultado_geom = results[0].geometryType
									this.resultados = [];
									var all_items_eq = [];
									for (var i = 0; i < results.length; i++) {
										this.resultados.push(results[i].features);
									}
									for (var i = 0; i < this.resultados.length; i++) {
										for (var j = 0; j < this.resultados[i].length; j++) {
											all_items_eq.push(new this.Entidad(this.resultados[i][j], i));
										}
									}
									this.elementos_eq = all_items_eq;								
									var globalid_selected ="globalid IN (";
									for (i = 0; i < this.elementos_eq.length; i++) {        
										globalid_selected += "'"+this.elementos_eq[i].features.attributes.globalid+"',"; 
									}
									globalid_selected += "'')";
									var query_g = new Query();							
									query_g.where = globalid_selected;
									query_g.outFields = ["*"];
									query_g.returnGeometry = true
											

									if(!this.id_eq.includes("VisorCartografico")){
										this.map._layers[this.id_eq].selectFeatures(query_g,FeatureLayer.SELECTION_NEW,(result_select_graphics) => {
											this.Graphics_eqd.clear();
											for (var i = 0; i < result_select_graphics.length; i++) {
												if (resultado_geom == 'esriGeometryPolyline'){
													var line = new SimpleLineSymbol();
													line.setWidth(5);
													line.setColor(new Color([0, 197, 255, 1]));
													result_select_graphics[i].symbol = line;
													this.Graphics_eqd.add(result_select_graphics[i]);
													if(i == result_select_graphics.length - 1){
														this.Graphics_eqd.clear();
														for (var i = 0; i < result_select_graphics.length; i++) {
															this.Graphics_eqd.add(result_select_graphics[i]);
														}
													}							  								  	
												}
											}			
											this.Graphics_eqd.redraw();	
											document.getElementById('option1').checked = '';															    	
										}
										,(rollback_select_graphics) => {
													this.Graphics_eqd.clear();
										});	

									}else if(this.map._layers[this.id_eq].layerInfos[this.config.idNumericoViario_VisorCartografico] && this.map._layers[this.id_eq].layerInfos[this.config.idNumericoViario_VisorCartografico].name == this.entidadEq){
										// Añadir capa de viario desde un FeatureServer
										var viario_FL = new FeatureLayer(this.config.url_ViarioFL,{id: "CapaViarioTemporal", outFields: ["*"]});

											this.map.addLayer(viario_FL);

											viario_FL.selectFeatures(query_g,FeatureLayer.SELECTION_NEW,(result_select_graphics) => {
											this.Graphics_eqd.clear();

											// Cambiar las coordenadas creando un gráfico y añadiendo result_select_graphics
											var graphicsLayer_Coord = new GraphicsLayer();
											var desiredSpatialReference = new SpatialReference({ wkid: 102100 });
											var geometria_select = result_select_graphics[0].geometry.paths
											var myLine ={geometry: {geometria_select,
														"spatialReference":{"wkid":102100}},
														"symbol":{"color":[0,0,0,255],"width":1,"type":"esriSLS","style":"esriSLSSolid"}};



											var graphic_coord = new Graphic({geometry: myLine});
											graphicsLayer_Coord.add(graphic_coord);

											for (var i = 0; i < graphicsLayer_Coord.graphics.length; i++) {
												if (resultado_geom == 'esriGeometryPolyline'){
													var line = new SimpleLineSymbol();
													line.setWidth(5);
													line.setColor(new Color([0, 197, 255, 1]));
													graphicsLayer_Coord.graphics[i].symbol = line;
													this.Graphics_eqd.add(graphicsLayer_Coord.graphics[i]);
													if(i == graphicsLayer_Coord.length - 1){
														this.Graphics_eqd.clear();
														for (var i = 0; i < graphicsLayer_Coord.length; i++) {
															var sr = new SpatialReference(102100);
															this.Graphics_eqd.add(graphicsLayer_Coord[i]);
														}
													}							  								  	
												}
											}






											// for (var i = 0; i < result_select_graphics.length; i++) {
											// 	if (resultado_geom == 'esriGeometryPolyline'){
											// 		var line = new SimpleLineSymbol();
											// 		line.setWidth(5);
											// 		line.setColor(new Color([0, 197, 255, 1]));
											// 		result_select_graphics[i].symbol = line;
											// 		this.Graphics_eqd.add(result_select_graphics[i]);
											// 		if(i == result_select_graphics.length - 1){
											// 			this.Graphics_eqd.clear();
											// 			for (var i = 0; i < result_select_graphics.length; i++) {
											// 				var sr = new SpatialReference(102100);
											// 				this.Graphics_eqd.add(result_select_graphics[i]);
											// 			}
											// 		}							  								  	
											// 	}
											// }
													
											this.Graphics_eqd.redraw();	
											document.getElementById('option1').checked = '';
											if(this.map._layers["CapaViarioTemporal"]){
												this.map.removeLayer(this.map._layers["CapaViarioTemporal"])
											}													    	
										}
										,(rollback_select_graphics) => {
													this.Graphics_eqd.clear();
										});	

									}

									

									this.load_table();
								}).catch(error => {
									this.Graphics_eqd.clear();
									// document.getElementById('select_polygon').innerHTML = "Seleccionar en el Mapa";
									document.getElementById('entidad_list').innerHTML = '<p class="t2_red">Se produjo un error, selecciona la geometría nuevamente</p>';
								})
						} else {
							// document.getElementById('select_polygon').innerHTML = "Seleccionar en el Mapa";
							document.getElementById('c').style.display = 'block';
						}
				} else if (document.getElementById('option2').checked == true){
					this.draw.deactivate();
					this.geometry_eq = event.geometry;
					document.getElementById('geometry_oblig').style.display = 'none';
					document.getElementById('select_region').style.display = 'none';
					document.getElementById('least_one_list').style.display = 'none';	
					document.getElementById('layer_novisible').style.display = 'none';	
					document.getElementById('entidad_list').innerHTML = '';	
					var EntidadSeleccionada = document.getElementById("selec_entidad").value;
					this.entidadEq = EntidadSeleccionada;
					var consultas = [];
					var url_capa = "";
					var id_capa = "";
					var pos_entidad = 0;
					for(var x=0; x < this.map.itemInfo.itemData.operationalLayers.length; x++){
						for(var y=0; y < this.map.itemInfo.itemData.operationalLayers[5].layers.length; y++){
							if(this.map.itemInfo.itemData.operationalLayers[x].title == this.entidadEq){
								url_capa = this.map.itemInfo.itemData.operationalLayers[x].url
								id_capa = this.map.itemInfo.itemData.operationalLayers[x].id
								break
							}
							if(this.map.itemInfo.itemData.operationalLayers[5].layers[y].name == this.entidadEq){
								url_capa = this.config.urlViario_VisorCartografico
								id_capa = this.config.idViario_VisorCartografico
								break
							}
						}	
					}
					this.url_eq = url_capa;
					this.id_eq = id_capa;

				
					if (this.map._layers[this.id_eq] && this.map._layers[this.id_eq].visible == true || this.map._layers[this.id_eq].layerInfos[this.config.idNumericoViario_VisorCartografico] && this.map._layers[this.id_eq].layerInfos[this.config.idNumericoViario_VisorCartografico].name == this.entidadEq) {
								var query = new Query();
								query.geometry = this.geometry_eq;
								query.where = "1=1";
								query.outFields = ["*"];
								query.returnGeometry = true;																	
								var query_elements = new QueryTask(url_capa);
								consultas.push(query_elements.execute(query));
								
								Promise.all(consultas).then(results => {
									const resultado_geom = results[0].geometryType
									var all_items_eq = [];
									for (var i = 0; i < results.length; i++) {
										this.resultados.push(results[i].features);
									}
									for (var i = 0; i < this.resultados.length; i++) {
										for (var j = 0; j < this.resultados[i].length; j++) {
											all_items_eq.push(new this.Entidad(this.resultados[i][j], i));
										}
									}
									this.elementos_eq = all_items_eq;								
									var globalid_selected ="globalid IN (";
									for (i = 0; i < this.elementos_eq.length; i++) {        
										globalid_selected += "'"+this.elementos_eq[i].features.attributes.globalid+"',"; 
									}
									globalid_selected += "'')";
									var query_g = new Query();							
									query_g.where = globalid_selected;
									query_g.outFields = ["*"];
									query_g.returnGeometry = true


									if(!this.id_eq.includes("VisorCartografico")){
										this.map._layers[this.id_eq].selectFeatures(query_g,FeatureLayer.SELECTION_ADD,(result_select_graphics) => {
											this.Graphics_eqd.clear();
											for (var i = 0; i < result_select_graphics.length; i++) {
												if (resultado_geom == 'esriGeometryPolyline'){
													var line = new SimpleLineSymbol();
													line.setWidth(5);
													line.setColor(new Color([0, 197, 255, 1]));
													result_select_graphics[i].symbol = line;
													this.Graphics_eqd.add(result_select_graphics[i]);
													if(i == result_select_graphics.length - 1){
														this.Graphics_eqd.clear();
														for (var i = 0; i < result_select_graphics.length; i++) {
															this.Graphics_eqd.add(result_select_graphics[i]);
														}
													}							  								  	
												}
											}			
											this.Graphics_eqd.redraw();	
											document.getElementById('option1').checked = '';															    	
										}
										,(rollback_select_graphics) => {
													this.Graphics_eqd.clear();
										});	

									}else if(this.map._layers[this.id_eq].layerInfos[this.config.idNumericoViario_VisorCartografico] && this.map._layers[this.id_eq].layerInfos[this.config.idNumericoViario_VisorCartografico].name == this.entidadEq){
										// Añadir capa de viario desde un FeatureServer
										var viario_FL = new FeatureLayer(this.config.url_ViarioFL,{id: "CapaViarioTemporal", outFields: ["*"]});

											this.map.addLayer(viario_FL);

											viario_FL.selectFeatures(query_g,FeatureLayer.SELECTION_ADD,(result_select_graphics) => {
											this.Graphics_eqd.clear();
											for (var i = 0; i < result_select_graphics.length; i++) {
												if (resultado_geom == 'esriGeometryPolyline'){
													var line = new SimpleLineSymbol();
													line.setWidth(5);
													line.setColor(new Color([0, 197, 255, 1]));
													result_select_graphics[i].symbol = line;
													this.Graphics_eqd.add(result_select_graphics[i]);
													if(i == result_select_graphics.length - 1){
														this.Graphics_eqd.clear();
														for (var i = 0; i < result_select_graphics.length; i++) {
															this.Graphics_eqd.add(result_select_graphics[i]);
														}
													}							  								  	
												}
											}		
											this.Graphics_eqd.redraw();	
											document.getElementById('option1').checked = '';
											if(this.map._layers["CapaViarioTemporal"]){
												this.map.removeLayer(this.map._layers["CapaViarioTemporal"])
											}													    	
										}
										,(rollback_select_graphics) => {
													this.Graphics_eqd.clear();
										});	

									}

									
									this.load_table();
								}).catch(error => {
									this.Graphics_eqd.clear();
									// document.getElementById('select_polygon').innerHTML = "Seleccionar en el Mapa";
									document.getElementById('entidad_list').innerHTML = '<p class="t2_red">Se produjo un error, selecciona la geometría nuevamente</p>';
								})
						} else {
							// document.getElementById('select_polygon').innerHTML = "Seleccionar en el Mapa";
							document.getElementById('layer_novisible').style.display = 'block';
						}

				}					
			},
			
			load_table: function () {
				this.tablaPos_ini =	[];	
				if (this.elementos_eq.length == 0) {
					document.getElementById('select_region').style.display = 'block';
				}
				else {
					document.getElementById('entidad_list').style.display = 'block';
					var ul=DomConstruct.toDom("<ul class='list-group'>");
				  DomConstruct.place(ul,'entidad_list','last');							
					for (var i = 0; i < this.elementos_eq.length; i++) {
						var li=DomConstruct.toDom("<li class='list-group-item'>");
						var input=DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
						on(input,'click',lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid,i,"check_btn_"))); 
						var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + Number(this.elementos_eq[i].features.attributes.Shape__Length.toFixed(2)) + " (m) | " + this.elementos_eq[i].features.attributes.globalid + "</span>");
						on(span,'click',lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid,i,"check_btn_")));
						DomConstruct.place(input,li,'last');
						DomConstruct.place(span,li,'last');
						DomConstruct.place(li,ul,'last');
						this.tablaPos_ini[i] = new this.Tabla_localizador(i,this.elementos_eq[i].features.attributes.globalid);
					}
					this.tablaPos_base =	this.tablaPos_ini;				
				}				
			},
			
		
			reseteo: function () {				
				this.elementos_eq = [];			
				this.geometry_eq = '';
				this.Graphics_eqd.clear();				
				document.getElementById('selec_entidad').value = "";
				// document.getElementById('select_polygon').style.display = 'none';
				document.getElementById('option1').checked = '';
				document.getElementById('option2').checked = '';
				document.getElementById('option1').disabled = false;
				document.getElementById('entidad_list').innerHTML = '';							
				document.getElementById('end').style.display = 'none';
				document.getElementById('btn_at_fi_4').style.display = 'flex';
				document.getElementById('loading_msg').innerHTML = '';
				document.getElementById('nombre_doc').value = '';
				document.getElementById('descripcion_doc').value = '';
				document.getElementById('idioma_doc').value = '38';
				document.getElementById('uso_interno_doc').value = '0';
				document.getElementById('editorial_doc').value = '';
				document.getElementById('visible_doc').value = '0';
				document.getElementById('codigo_anterior_doc').value = '';				
				document.getElementById('nombre_anterior_doc').value = '';
				this._uploadField.value = '';
			},
			
			load_page: function (num) {
				document.getElementById('inicio').style.display = 'none';
				document.getElementById('form_1').style.display = 'none';
				document.getElementById('form_2').style.display = 'none';
				document.getElementById('foot_1').className = 'dot';
				document.getElementById('foot_2').className = 'dot';
				document.getElementById('foot_3').className = 'dot';
				document.getElementById('foot_4').className = 'dot';
				if (num > 0 && num <= 4) {
					document.getElementById('footer').style.display = 'block';
				}
				else {
					document.getElementById('footer').style.display = 'none';
					if (num == 0) { document.getElementById('inicio').style.display = 'block'; }
				}
				switch (num) {
					case 1:
						document.getElementById('foot_1').className = 'dot_check';
						document.getElementById('form_1').style.display = 'block';
						break;
					case 2:
						document.getElementById('foot_2').className = 'dot_check';
						document.getElementById('form_2').style.display = 'block';
						break;
				}
			},
			
			Tabla_localizador: function (posicion, globalid) {
				this.posicion = posicion;
				this.globalid = globalid;
			},
			
			Entidad: function (features, index) {
				this.features = features;
				this.index = index;
			}
			
		});
	});


